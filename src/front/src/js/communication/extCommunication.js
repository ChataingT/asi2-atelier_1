//https://www.fullstackreact.com/30-days-of-react/day-21/
import axios from "axios";

export const ExtCommMiddleware = (store) => (next) => (action) => {
    // Our middleware
    if(action.type === "SUBMIT_USER_ACTION"){
        let new_user={};
        //new_user.id=action.user.id;
        new_user.surname=action.user.surname;
        new_user.lastname=action.user.lastname;
        new_user.login=action.user.login;
        new_user.pwd=action.user.pwd;
        //new_user.img=action.user.img;
        new_user.account=action.user.money;

        //add user to remote server
        axios.post("http://127.0.0.1:8082/user", new_user)
            .then(function (data) {
            }.bind(this))
            .catch(function (error) {
            });

    }

    // call the next function
    next(action);
}
  