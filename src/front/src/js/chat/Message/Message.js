import React, { Component } from 'react';

class Message extends Component
{
    render() {
        let message_ribbon_classes = "ui";
        let message_ribbon_content;
        
        if (this.props.message.received)
        {
            message_ribbon_content = this.props.message.source;
            message_ribbon_classes += " blue"
        }
        else
        {
            message_ribbon_content = "Me";
            message_ribbon_classes += " green right"
        }
        
        message_ribbon_classes += " ribbon label";
        
        return (
            <div className="ui raised segment">
                <a className={message_ribbon_classes}>
                    {message_ribbon_content}
                </a>
                <span>{this.props.message.date}</span>
                <p>{this.props.message.content}</p>
            </div>
        );
    }
}

//export the current classes in order to be used outside
export default Message;