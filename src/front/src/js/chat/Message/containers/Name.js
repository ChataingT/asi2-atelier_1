import React, { Component } from 'react';

class Name extends Component {
    constructor(props) {
        super(props);        
    }
  
  render() {
    return (

        <a className="ui blue ribbon label">{this.props.name}</a>

    );
  }
}

export default Name;