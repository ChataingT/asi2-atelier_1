import React, { Component } from 'react';

class Content extends Component {
    constructor(props) {
        super(props);        
    }
  
  render() {
    return (
        <div>
            <span>{this.props.time}</span>
            <p>{this.props.content}</p>
        </div>
    );
  }
}

export default Content;