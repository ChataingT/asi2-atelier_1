import React, { Component } from 'react';

import * as config from '../config';
import {Dropdown} from "semantic-ui-react";
import ListMessages from "../ListMessages/ListMessages";
import moment from "moment";

/**
 * Interface to easily send information with socket.io for the chat
 */
export class Chat extends Component
{
    constructor(props)
    {
        super(props);
    
        this.state = {
            error: null,
            opponents: [],
            messages_list: [],
            opponent_name: null,
            user_name: props.user_name
        };
        
        this._socket_connection = props.socket_connection;
        this._message_dest = null;

        console.log(this.state.user_name, this.props.user_id);

        this.sendUserId(props.user_id);
    
        this._socket_connection.subscribeCallback(
            config.SOCKET_EVENT_TYPES.USER_LIST_SENT,
            (msg) => {
                this.setState({
                    opponents: this.parseUserList(msg)
                });
            }
        );
        
        this._socket_connection.subscribeCallback(
            config.SOCKET_EVENT_TYPES.MESSAGE_DISTRIBUTED,
            this.messageReceived.bind(this)
        );
        
        this._socket_connection.subscribeCallback(
            config.SOCKET_EVENT_TYPES.ERROR,
            error => this.setState({error: error})
        );
        
        this.sendUserId = this.sendUserId.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.parseUserList = this.parseUserList.bind(this);
        this.selectMessageDestination = this.selectMessageDestination.bind(this);
        this.messageReceived = this.messageReceived.bind(this);
        this.displayMessage = this.displayMessage.bind(this);
    
        this.handleClick=this.handleClick.bind(this);
    }
    
    /**
     * Send the id of the current user to the backend
     *
     * @param user
     */
    sendUserId(id) {
        this._socket_connection.emit(
            config.SOCKET_EVENT_TYPES.USER_ID_SENT,
            id
        );
    }
    
    /**
     * Send a new message to the socket backend
     *
     * @param message
     */
    sendMessage(message) {
        this._socket_connection.emit(
            config.SOCKET_EVENT_TYPES.MESSAGE_SENT,
            Object.assign({
                dest: this._message_dest,
                source: this.state.user_name
            }, message)
        );
    }
    
    messageReceived(message) {
        this.displayMessage(
            message,
            true
        );
    }
    
    
    selectMessageDestination(socket_id, name) {
        this._message_dest = socket_id;
        this.setState({
            opponent_name: name,
            messages_list: []
        });
    }
    
    handleClick()
    {
        let message = {
            date: moment().format('dd-mm-YY'),
            content: document.getElementById('chat-message-text').value,
        };
        
        // TODO maybe the sendMessage can return a promise resolved when
        //      message is distributed?
        this.sendMessage(message);
        
        this.displayMessage(message, false);
    };
    
    displayMessage(message, received)
    {
        let message_list = [];
        
        this.state.messages_list.forEach(message => message_list.push(message));
    
        message_list.push(Object.assign({
            received: received
        }, message));
    
        this.setState({
            messages_list: message_list
        });
    }
    
    parseUserList(user_list)
    {
        let ret;
        ret = JSON.parse(user_list);
        delete ret[this._socket_connection.socket_id];
        
        let entries = Object.entries(ret);
        
        //Set a default destination
        if(
            (!this._message_dest
            || !ret[this._message_dest])
        )
        {
            if(entries.length > 0)
            {
                this.selectMessageDestination(entries[0][0], entries[0][1]._username);
            }
        }

        return ret;
    }
    
    /**
     * @returns {*}
     */
    render()
    {
        let user_dropdown_list = Object.entries(this.state.opponents).map(entry => {
            let [socket, opponent] = entry;
            
            return {
                key: socket,
                value: socket,
                text: opponent._username
            };
        });
        
        if(this.state.error)
        {
            return (
                <div>
                    <h1>Chat non disponible</h1>
                    <p>{this.state.error}</p>
                </div>
            );
        }
        
        return (
            <div>
                <div className="ui segment">
                    <div className="ui top attached label">
                        <div className="ui two column grid">
                            <div className="column">Chat</div>
                            <div className="column">
                                <div className="ui two column grid">
                                    <div
                                        className="column">{this.props.opponent}</div>
                                    <div className="column">
                                        <i className="user circle icon"/>
                                        {this.props.user_name}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Dropdown
                    placeholder='Select User'
                    options={user_dropdown_list}
                    onChange={
                        (e, data) => this.selectMessageDestination(
                            data.value,
                            e.target.textContent
                        )
                    }
                    fluid
                    search
                    selection
                />
                <ListMessages
                    messages_list={this.state.messages_list}
                    opponent_name={this.state.opponent_name}
                />
                <div className="ui form">
                    <div className="field">
                        <textarea id="chat-message-text" rows="2" />
                    </div>
                </div>
                <button
                    className="fluid ui right labeled icon button"
                    onClick={this.handleClick}
                    disabled={this.state.opponent_name === null}
                >
                    <i className="right arrow icon" />
                    Send
                </button>
            </div>
        );
    }
}