import React, { Component } from 'react';

import { connect } from 'react-redux';

import Message from '../Message/Message';

export default class ListMessages extends Component
{
    //render function use to update the virtual dom
    render() {
        // evite d'appeler mapStateToProps si messageList est vide
        if(this.props.messages_list === undefined) {
            return (<div />);
        }
    
        let array_value = this.props.messages_list;

        return (
            <div className="ui segment">  
            { array_value.map(item =>
                (<Message
                    message={item}
                />))
            }
            </div>
        );
    }
}
