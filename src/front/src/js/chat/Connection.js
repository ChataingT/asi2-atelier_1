import io from 'socket.io-client';

import * as config from './config';

/**
 * Interface used for socket connexion
 */
export class Connection
{
    constructor() {
        this._socket = io.connect(
            config.SOCKET_BACKEND_URL
            + ':'
            + config.SOCKET_BACKEND_PORT
        );
    
        this._message_received_callback = new Map();
        
        this.subscribeCallback = this.subscribeCallback.bind(this);
        this.eventCallback = this.eventCallback.bind(this);
    }
    
    /**
     * Fire an event of the defined topic with a specific message
     */
    emit(event_type, msg)
    {
        this._socket.emit(event_type, msg);
    }
    
    /**
     * Register a method that will be called when socket.io fire an event
     * with the desired type
     *
     * @param event_type the type of the socket.io event
     * @param callback the callback to register
     */
    subscribeCallback(event_type, callback) {
        if(!this._message_received_callback.has(event_type))
        {
            this._message_received_callback.set(event_type, []);
            this._socket.on(
                event_type,
                msg => {
                    this.eventCallback(event_type, msg)
                }
            );
        }
        
        this._message_received_callback
            .get(event_type)
            .push(callback);
    }
    
    /**
     * Generic method to call each registered callback when a registered
     * event is fired
     */
    eventCallback(event_type, msg) {
        this._message_received_callback
            .get(event_type)
            .forEach(callback => callback(msg));
    }
    
    get socket_id()
    {
        return this._socket.id;
    }
}
