import React, { Component } from 'react';
import CardList from "../store/CardList";
import Card from "../card/Card";

export default class SellPage extends Component {
    render() {
        return (
            <div className="ui two column grid">
                <div className="row">
                    <div className ="column">
                        <div className="col-md-4 col-lg-4" >
                            <CardList
                                display_type = "row"
                                storeChoice="sell" />
                        </div>
                    </div>
                    <div className = "column">
                        <div className="col-md-4 col-lg-4" >
                        <Card  display_type='detailed' card = "" ></Card>

                        </div>
                    </div>

                </div>
            </div>
        );
    }
}