import React, { Component } from 'react';
import CardList from "../store/CardList";
import {Chat} from "../chat/Chat/Chat";
import GameZone from "../game/GameZone";
import HalfGameZone from "../game/HalfGameZone";

export default class BuyPage extends Component {

    render() {
        return (
            <div className="ui twelve column grid">
                <div className="row">
                    <div className="column four wide">
                        <Chat
                            user_name={this.props.user_name}
                            user_id={this.props.user_id}
                            socket_connection={this.props.socket_connection}
                        />
                    </div>
                    <div className="column eight wide">
                        <GameZone
                            player_name = {this.props.user_name}
                            card_list = {this.props.opponent_card_list}
                        />
                    </div>
                </div>
            </div>
        );
    }
}