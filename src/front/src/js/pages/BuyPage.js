import React, { Component } from 'react';
import CardList from "../store/CardList";
import Card from "../card/Card";


export default class BuyPage extends Component {
    constructor(props) {
        super(props); 
        this.state = props;
    }

    render() {
        return (
            <div className="ui two column grid">
                <div className="row">
                    <div className ="column">
                        <div className="col-md-4 col-lg-4" >
                            <CardList
                                display_type="row"
                                storeChoice="buy"
                                money={this.props.money}>
                            </CardList>
                        </div>
                    </div>
                    <div className = "column">
                        <div className="col-md-4 col-lg-4" >
                        <Card  display_type='detailed' card = "" ></Card>

                        </div>
                    </div>

                </div>
            </div>
        );
    }
}