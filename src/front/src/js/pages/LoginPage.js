import React, { Component } from 'react';
import UserSignInForm from "../user/UserSignInForm";
import UserLogInForm from "../user/UserLogInForm";

export default class LoginPage extends Component {

    render() {
        return (
            <div className="middle aligned row">
                <div className="column">
                    <h1 className="ui centered aligned header"
                        style={{marginTop:"2em"}}>
                        Welcome to CardWorld !
                    </h1>
                    <div className="ui grid container" id="login-form-container">
                        <div className="eight wide column">
                            <UserLogInForm />
                        </div>
                        <div className="eight wide column">
                            <UserSignInForm/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}