import React, { Component } from 'react';
import CardList from "../store/CardList";
import Card from "../card/Card";
import { connect } from 'react-redux';

class ProfilePage extends Component {
     
    render() {
        return (
            <div className="ui grid">
                <div className="row">
                    <div className="ten wide column">
                        <CardList display_type='short'/>
                    </div>
                    <div className="six wide column">
                        <Card display_type='detailed' card = "" />
                    </div>
                </div>
            </div>
        );
    }
}



export default  ProfilePage;