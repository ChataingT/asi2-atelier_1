import React from 'react';
import Page from './page';
class PageShortDisplay extends React.Component{
        
    constructor(props) {
        super(props);
        this.state={
            name:this.props,
            description:"",
            image:""
        }
    }
    render() {
        let name = "";
        let description = "";
        let image = "";
        Object.keys(Page).forEach(element => { // read the json and iterate on it
            if (Page[element].name === this.props.name) {
                name = Page[element].name;
                description = Page[element].description;
                image = Page[element].image;
            }
            
        });
                
        return (
            <div className="ui two column grid">
                <div className="columne">
                    <img src={image} alt="$_$"></img>
                </div>
                <div className="columne">
                    <div className="content">
                        <h1>{name}</h1>
                        <h3>{description}</h3>
                    </div>
                </div>
            </div>
        );
    }
}
export default PageShortDisplay;
