import React from 'react';
import {routing} from '../action';
import {connect} from 'react-redux';

import ROUTES from '../../routes';


class Button extends React.Component{

    constructor(props) {
        super(props);
    }

    reaction(type){
        this.props.dispatch(routing(type)) // redux send info to all
    }

    render() {

        if (this.props.currentPage === ROUTES.PROFILE){

            return (
                <div className="ui three column center aligned grid">
                    <div className="column">
                        <button className="ui button" tabIndex="1" onClick={() => this.reaction(ROUTES.BUY)}>BUY</button>
                    </div>
                    <div className="column">
                        <button className="ui button" tabIndex="1" onClick={() => this.reaction(ROUTES.SELL)}>SELL</button>
                    </div>
                    <div className="column">
                        <button className="ui button" tabIndex="1" onClick={() => this.reaction(ROUTES.PLAY)}>PLAY</button>
                    </div>
                </div>
            );
        }
        else {
            return(
                <div className="ui button" tabIndex="1" onClick={(data)=>this.reaction(ROUTES.PROFILE)}>Profil</div>
            );
        }
    }
}
export default connect() (Button);
