import ROUTES from "../../routes";

export const pageReducer= (page=ROUTES.LOGIN, action) => {

    switch (action.type) {

        case 'SELECT_CURRENT_USER':
            return ROUTES.PROFILE;

        case 'ROUTING':
            return action.obj;

        default:
            return page;
    }
};

export default pageReducer;


