import { combineReducers } from 'redux';
import userReducer from '../user/reducer/userReducer';
import pageReducer from '../page/pageReducer';
import cardReducer from '../card/reducers/cardReducer';
import storeReducer from '../store/reducers/storeReducer';


const globalReducer = combineReducers({
    userReducer: userReducer,
    pageReducer: pageReducer,
    cardReducer: cardReducer,
    storeReducer: storeReducer
});

export default globalReducer;
