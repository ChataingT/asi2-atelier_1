import React from 'react';
import { connect } from 'react-redux';
import UserSimpleDisplay from './userSimpleDisplay';
import PageShortDisplay from '../page/pageShortDisplay';
import Button from '../page/button';

class Header extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
            user:this.props.selectedUser,
        };
    }

    render() {

        return(
            <div className="ui three column relaxed grid">
                <div className="justified column">
                    <div className="ui segment">
                        <PageShortDisplay
                        name={this.props.page}
                        ></PageShortDisplay>
                    </div>
                </div>
                <div className="center aligned column">
                    <Button currentPage=""></Button>
                </div>
                <div className="column">
                    <div className="ui justified segment">
                        <UserSimpleDisplay
                        username={this.state.user.username}
                        money={Math.round(this.state.user.money)}
                        image={this.state.user.image}
                        ></UserSimpleDisplay>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;
