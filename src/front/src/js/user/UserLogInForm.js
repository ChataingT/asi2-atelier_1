import React from 'react';
import { connect } from 'react-redux';
import { updateUser } from '../action';
import axios from 'axios';

class UserLogInForm extends React.Component{

    constructor(props) {
        super(props);

        //TODO Remove the default JF
        this.state = {
            username: "",
            pwd: "",
            wrongEntry: ""
        };

        this.processInput = this.processInput.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(data){
        this.setState({
            currentUser_username: data.username,
            currentUser_pwd: data.pwd,
        });
    }

    processInput(event){
        const target = event.currentTarget;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let currentVal=this.state;

        this.setState({
            [name]: value
        });

        currentVal[name]= value;

        this.handleChange(currentVal);
    }

    submitOrder(e) {

        let user = {
            username: this.state.username,
            pwd: this.state.pwd,
            money: 1000
        };

        axios.get(`http://127.0.0.1:8082/auth?login=${user.username}&pwd=${user.pwd}`).then( function (data) {
            if (data.data === true) {
                axios.get(`http://127.0.0.1:8082/user/${user.username}/${user.pwd}`).then(
                    function (data) {
                        axios.get(`http://127.0.0.1:8082/user/${data.data}`).then(
                            function (data) {
                                this.state.wrongEntry = "";

                                let newUser = {
                                    username: data.data.login,
                                    id: data.data.id,
                                    money: data.data.account,
                                    cardList: data.data.cardList,
                                    image: data.data.image
                                };

                                this.props.dispatch(updateUser(newUser))
                            }.bind(this))
                            .catch(function (error) {
                                console.error(error);
                            });
                    }.bind(this))
                    .catch(function (error) {
                        console.error(error);
                    });
            }
            else{
                this.setState({
                    wrongEntry:"Authentification failed, user unknown !"
                })
            }
        }.bind(this))
            .catch(function (error) {
                console.error(error);
            });

        e.preventDefault();
        return false;
    }


    render() {
        return (
            <form
                className="ui form"
                onSubmit={(e) => this.submitOrder(e)}

            >
                <h4 className="ui dividing header">Log in</h4>
                <div className="field">
                    <label>Username</label>
                    <input
                        type="text"
                        name="username"
                        placeholder="Username"
                        onChange={(ev) => this.processInput(ev)}
                        value={this.state.username}
                    />
                </div>
                <div className="field">
                    <label>Password</label>
                    <input
                        type="password"
                        name="pwd"
                        placeholder="******"
                        onChange={(ev) => this.processInput(ev)}
                        value={this.state.pwd}
                    />
                </div>
                <div>
                    <h5>
                        {this.state.wrongEntry}
                    </h5>
                </div>
                <input type="submit" className="ui button" tabIndex="1" value="Connect" />
            </form>
        );
    }

}
export default connect() (UserLogInForm);
