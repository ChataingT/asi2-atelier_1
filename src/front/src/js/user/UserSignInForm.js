import React from 'react';
import { connect } from 'react-redux';
import { updateUser } from '../action';
import axios from 'axios';

class UserSignInForm extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            pwd: "",
            wrongEntry: "",
            pwd_confirm: ""
        };

        this.processInput = this.processInput.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(data){
        this.setState({
            username: data.username,
            pwd: data.pwd,
            pwd_confirm: data.pwd_confirm
        });
    }

    processInput(event){
        const target = event.currentTarget;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let currentVal=this.state;

        this.setState({
            [name]: value
        });

        currentVal[name]= value;

        this.handleChange(currentVal);
    }

    submitOrder(e) {
        
        let user = {
            username: this.state.username,
            pwd: this.state.pwd,
            money: 1000
        };
        let errorMsg="";
        if (user.pwd == this.state.pwd_confirm){ 
            axios.get(`http://127.0.0.1:8082/auth?login=${user.username}&pwd=${user.pwd}`).then( function (data) {
                if (data.data !== true) {
                        axios.post(`http://127.0.0.1:8082/user`, {
                                                            "surname":"a",
                                                            "lastname":"a",
                                                            "login":user.username,
                                                            "pwd":user.pwd,
                                                            "img":"",
                                                            "account":user.money
                                                            }
                                ).then( function(data) {
                                    if (data.status === 200) {
                                        errorMsg="Your count has been created. You can now connect";}
                                    else{
                                        errorMsg="Failed in creating"
                                    }
                                })
                            }
                    else{
                        errorMsg="Compte already existing"
                    }})}
        else{
            errorMsg="Error ! The passwords are differents."
        }
        this.setState({
            wrongEntry:errorMsg
        });
        e.preventDefault();
        return false;

    }


    render() {
        return (
            <form
                className="ui form"
                onSubmit={(e) => this.submitOrder(e)}
            >
                <h4 className="ui dividing header">Sign in</h4>
                <div className="field">
                    <label>Username</label>
                    <input
                        type="text"
                        name="username"
                        placeholder="Username"
                        onChange={(ev) => this.processInput(ev)}
                        value={this.state.username}
                    />
                </div>
                <div className="field">
                    <label>Password</label>
                    <input
                        type="password"
                        name="pwd"
                        placeholder="******"
                        onChange={(ev) => this.processInput(ev)}
                        value={this.state.pwd}
                    />
                </div>
                <div className="field">
                    <label>Confirm password</label>
                    <input
                        type="password"
                        name="pwd_confirm"
                        placeholder="******"
                        onChange={(ev) => this.processInput(ev)}
                        value={this.state.pwd_confirm}
                    />
                </div>
                <div>
                    <h5>
                        {this.state.wrongEntry}
                    </h5>
                </div>
                <input type="submit" className="ui button" tabIndex="1" value="Connect"/>
            </form>

        );
    }

}
export default connect() (UserSignInForm);
