var axios=require('axios');

/*
script to init the backend's database with 3users
*/


function addUser (user) {
          //add user to remote server
    axios.post("http://127.0.0.1:8082/user", user)
    .then(function (data) {
     }.bind(this))
     .catch(function (error) {
     });

 }


let user1={};
user1.surname="John";
user1.lastname="Doe";
user1.login="JDoe";
user1.pwd="JDoe";
user1.img="https://www.clubpoker.net/forum-poker/uploads/monthly_2015_08/john-doe.jpg.a7ac051111ba6d8b0c49ae324c944755.jpg";
user1.account=1523;

let user2={};
user2.surname="Jay";
user2.lastname="Finn";
user2.login="JF";
user2.pwd="JF";
user2.img="https://www.planetehockey.com/joueur-kobler-jay-finn,6084.jpg";
user2.account=666;

let user3={};
user3.surname="Bomb";
user3.lastname="Berman";
user3.login="BB";
user3.pwd="BB";
user3.img="https://images-na.ssl-images-amazon.com/images/I/61x3Dtmb-9L._SY355_.png";
user3.account=1;


export function  addUsers () {
    addUser(user1);
    addUser(user2);
    addUser(user3);
}

export default addUsers;

