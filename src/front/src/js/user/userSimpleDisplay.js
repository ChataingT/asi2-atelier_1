import React from 'react';

class UserSimpleDisplay extends React.Component{
        
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="ui two column grid">
                <div className="columne">
                    <img src={this.props.image} alt="@('_')@"></img>
                </div>
                <div className="columne">
                    <div className="content">
                        <h1>{this.props.username}</h1>
                        <h3>{this.props.money} $</h3>
                    </div>
                </div>
            </div>
        );
    }
}
export default UserSimpleDisplay;
