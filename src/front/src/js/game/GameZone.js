import React, { Component } from 'react';

import HalfGameZone from './HalfGameZone';

class GameZone extends Component {
	//class constructor whith given properties
	constructor(props) {
		super(props);
	}

	//render function use to update the virtual dom
	render() {
		return (
			<div>

				<HalfGameZone
					is_reversed = {false}
					player_name = {this.props.user_name}
					card_list = {this.props.opponent_card_list}
				/>

				<div className="row">
					<div className="ui grid ">
						<div className="twelve wide column">
							<h4 className="ui horizontal divider header">
								VS
							</h4>
						</div>
						<div className="four wide column">
							<button className="huge ui primary button">
								Attack
							</button>
						</div>
					</div>
				</div>

				<HalfGameZone
					is_reversed = {true}
					player_name = {this.props.player}
					card_list = {this.props.player_card_list}
				/>

			</div>

		);
	}
}

//export the current classes in order to be used outside
export default GameZone;
