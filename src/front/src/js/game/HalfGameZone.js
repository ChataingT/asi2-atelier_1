import React, { Component } from 'react';
import CardList from "../store/CardList";
import CardDisplay from "../card/CardDisplay";

class HalfGameZone extends Component {

    render() {

        let render_profil;
        
        if (this.props.is_reversed === false) {

            render_profil=(
                <div className="ui one  column centered grid">
                    <div className="row">
                        <div className="column"> <i className="user circle huge icon " /></div>
                    </div>
                    <div className="row">
                        <div className=" column">Eric Smith</div>
                    </div>     
                    <div className="row">
                        <div className="column">
                            <div className="ui teal progress" data-percent="74" id="progressBarId1" >
                                <div className="bar" />
                                <div className="label">Action Points</div>
                            </div>
                        </div>
                    </div>
                </div>
            );

        } else {

            render_profil=(
                <div className="ui one  column centered grid">
                    <div className="row">
                        <div className="column">
                            <div className="ui teal progress" data-percent="20" id="progressBarId2" >
                                <div className="label">Action Points</div>
                                <div className="bar" />
                            </div>
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className=" column">Me</div>
                    </div>     
                    <div className="row">
                        <div className="column"><i className="user circle huge icon " /></div>
                    </div>        
                        
                </div>
            );
        }

        return (
            <div className="row">
                <div className="ui grid">
                    <div className="two wide column">
                        {render_profil}
                    </div>
                </div>
            </div>

        );
    }
}

//export the current classes in order to be used outside
export default HalfGameZone;
