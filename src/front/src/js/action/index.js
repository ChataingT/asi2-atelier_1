export const updateUser =
    (user) => {
        return {
            type: 'SELECT_CURRENT_USER',
            obj: user
        };
    };

export const routing =
    (route) => {
        return {
            type: 'ROUTING',
            obj: route
        };
    };
