import React, { Component } from 'react';
import '../lib/Semantic-UI-CSS-master/semantic.min.css'
import { Provider } from 'react-redux';
import Header from './user/header';
import Button from './page/button';
import globalReducer from './reducer';
import { createStore, applyMiddleware } from 'redux';
import {ExtCommMiddleware} from  './communication/extCommunication';
import {loadCardList} from './store/actions/index';
import { init_list_card } from './init'
import BuyPage from "./pages/BuyPage";
import SellPage from "./pages/SellPage";
import ProfilePage from "./pages/ProfilePage";
import LoginPage from "./pages/LoginPage";
import ErrorPage from "./pages/ErrorPage";

import ROUTES from "../routes";

import {addUsers} from "./user/addUser/addUser";
import PlayPage from "./pages/PlayPage";
import {Connection} from "./chat/Connection";

class App extends Component {

    constructor(props) {
        super(props);

        let socket_connection = new Connection();
        //addUsers(); // send 3 request POST to add 3 user in the backend

        this.store = createStore(globalReducer,applyMiddleware(ExtCommMiddleware));
        this.store = createStore(globalReducer);

        this.state = {
            currentPage: ROUTES.LOGIN, // login - profil - buy - sell
            list_card: [],
            firstLoad: true,
            selectedUser: {
                id: null,
                username: null
            },
            socket_connection: socket_connection,
            store: this.store
        };

        this.store.subscribe(() => this.router());

        this.router = this.router.bind(this);

        init_list_card()
            .then((data) => {
                this.state.list_card=data;
            });
    }

    router() {

        let currentState = this.store.getState();
        this.changeSelectedUser(currentState.userReducer);

        if (
            currentState.userReducer !== undefined &&
            currentState.userReducer.username !== this.state.selectedUser.username
        ) {
            this.changeSelectedUser(currentState.userReducer);
        }
        else if (
            currentState.pageReducer !== undefined
        ) {
            this.changePage(currentState.pageReducer);
        }
    }

    changeSelectedUser(user) {
        this.setState({
            selectedUser: user,
        });

        this.changePage(ROUTES.PROFILE);
    }

    changePage(key) {

        if(key !== ROUTES.LOGIN && this.state.firstLoad) { // dispatch has to be done once
            this.state.firstLoad = false;

            this.store.dispatch(loadCardList({
                list_card_user: this.state.selectedUser.cardList,
                list_card: this.state.list_card
            }));

        }

        this.setState({
            currentPage: key
        });
    }

    //render function use to update the virtual dom
    render() {
        let page;

        let header;
        if(this.state.selectedUser.username !== null)
        {
            header = (
                <div className="container">
                    <div className="middle aligned row">
                        <Header
                            selectedUser={this.state.selectedUser}
                            page={this.state.currentPage}
                        >
                        </Header>
                    </div>
                    <Button
                        currentPage={this.state.currentPage}
                    />
                </div>
            )
        }

// TODO clean page BUY SELL
        switch (this.state.currentPage) {

            case ROUTES.PROFILE:
                page = <ProfilePage />;
                break;

            case ROUTES.BUY:
                page = <BuyPage
                            money={this.state.selectedUser.money} />;
                break;

            case ROUTES.SELL:
                page = <SellPage />;
                break;

            case ROUTES.LOGIN:
                page = <LoginPage />;
                break;

            case ROUTES.PLAY:
                page = <PlayPage
                    user_name={this.state.selectedUser.username}
                    user_id={this.state.selectedUser.id}
                    socket_connection={this.state.socket_connection}
                />
                break;

            default:
                page = <ErrorPage />;
                break;
        }

        return(
            <Provider store={ this.store }>
                {header}
                {page}
            </Provider>
        );
    }
}

//export the current classes in order to be used outside
export default App;
