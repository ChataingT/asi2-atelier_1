import React from 'react';
import ShortCardDisplay from './ShortCardDisplay';
import RowCardDisplay from './RowCardDisplay';
import CardDisplay from './CardDisplay';
import { connect } from 'react-redux';

export const CARD_DISPLAY_TYPES = {
    HIDDEN: 'hidden',
    SHORT: 'short',
    ROW: 'row',
    DETAILED: 'detailed'
};

class Card extends React.Component
{
    constructor(props)
    {
        super(props);
        
        this.state = {
            hp: props.card.hp,
            energy: props.card.energy,
            defence: props.card.defence,
            attack: props.card.attack,
            price: props.card.price,
            imgUrl: props.card.imgUrl,
            name: props.card.name,
            family: props.card.family,
            
            display_type: props.display_type,
            key: props.key
        }
    }
    
    render()
    {
        let display;
        
        switch (this.props.display_type)
        {
            case CARD_DISPLAY_TYPES.SHORT:
                display = <ShortCardDisplay card= {this.props.card}/>;
                break;
                
            case CARD_DISPLAY_TYPES.ROW:
                display = <RowCardDisplay card = {this.props.card} storeChoice = {this.props.storeChoice} money={this.props.money}/>;
                break;
    
            case CARD_DISPLAY_TYPES.DETAILED:
                display = <CardDisplay cardDescription = {this.props.card}/>;
                break;
                
            default:
                display = '';
        }
        
        return display;
    }
}

//export default  (Card);
/*const mapStateToProps = (state) => {
    return {
      cardDescription : state.cardReducer
    }
};

export default  connect(mapStateToProps)(Card);*/
export default Card;
