import React, { Component } from 'react';
import { connect } from 'react-redux';


import {setSelectedCard} from './actions';
import {sellOneCard, buyOneCard} from './../store/actions';


class RowCardDisplay extends Component {
    constructor(props) {
        super(props); 
        this.state ={
            page_to_display : this.props.page_to_display,
            storeChoice : this.props.storeChoice
        }
    }

    SellCard(card_obj){
        this.props.dispatch(sellOneCard(card_obj));
    }

    BuyCard(card_obj){
        this.props.dispatch(buyOneCard(card_obj));

    }

    handleOnCardSelected(card_obj){
        this.props.dispatch(setSelectedCard(card_obj));   
    }
  
    render() {
        
        let card_list;
        let iconToDisplay;
        let color;

        if (this.props.card.price > this.props.money){
            color="red";
        }
        else {
            color="";
        }

        let classUI = "ui vertical "+color+" animated button";

        switch (this.state.storeChoice){
            case "sell":
            iconToDisplay=<div className="ui vertical animated button" tabindex="0" onClick={()=> {this.SellCard (this.props.card)}}>
                <div className="hidden content">Sell</div> 
                <div className="visible content">
                    <i className="shop icon" ></i>

                </div>
            </div>
                break;

            case "buy":
            iconToDisplay=<div className={classUI} tabindex="0" onClick={()=> {this.BuyCard (this.props.card)}}>
                <div className="hidden content">Buy</div> 
                <div className="visible content">
                    <i className="shop icon" ></i>

                </div>
            </div>
                break;
            default:
                iconToDisplay = '';

        }


        return card_list =(
            <tr onClick={()=>{this.handleOnCardSelected(this.props.card)}}>                
                <td>
                    <img  className="ui avatar image" src={this.props.card.imgUrl} /><span>{this.props.card.name} </span>
                </td>
                <td>{this.props.card.description}</td>
                <td>{this.props.card.family}</td>
                <td>{Math.round(this.props.card.hp)}</td>
                <td>{Math.round(this.props.card.energy)}</td>
                <td>{Math.round(this.props.card.defence)}</td>
                <td>{Math.round(this.props.card.attack)}</td>
                <td>{Math.round(this.props.card.price)}$</td>
                <td>
                   {iconToDisplay}
                </td>  
            </tr>          
        );

       
                          
    }
}

export default connect()(RowCardDisplay);