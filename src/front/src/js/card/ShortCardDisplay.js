import React, { Component } from 'react';
import { connect } from 'react-redux';

import {setSelectedCard} from './actions';

class ShortCardDisplay extends Component {
    constructor(props) {
        super(props);

    }
    //Todo emetteur a redux
    handleOnCardSelected(card_obj){
        this.props.dispatch(setSelectedCard(card_obj));
    }

    render() {

        return (
            <div className="four wide column" onClick={()=>{this.handleOnCardSelected(this.props.card)}}>
                <div className="ui card">
                    <div className="content">
                        <div className="ui grid">
                            <div className="three column row">
                                <div className="column" >
                                    <a className="ui red circular label">{Math.round(this.props.card.hp)}</a>
                                </div>
                                <div className="column no-wrap text-center" >
                                    <h5>{this.props.card.name}</h5>
                                </div>
                                <div className="column">
                                    <a className="ui yellow circular label right floated">{this.props.card.energy}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="short-image-card" style={{backgroundImage: `url(${this.props.card.imgUrl})`}}/>
                </div>
            </div>
        );

        /*
        return (
            <div className="game-card" onClick={()=>{this.handleOnCardSelected(this.props.card)}}>
                <div className="image imageCard" style={{backgroundImage: `url(${this.props.card.imgUrl})`}}>
                </div>
            </div>
        );
         */
    }
}

export default connect() (ShortCardDisplay);