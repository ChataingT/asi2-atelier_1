import React, { Component } from 'react';
import { connect } from 'react-redux';

class CardDisplay extends Component {
    constructor(props) {
        super(props);
        this.state = {};

    }

    render() {
        let card_visual;
        let cardToDisplay ={};
        let style;
        if(Object.entries(this.props.cardDescription).length === 0 && this.props.cardDescription.constructor === Object){ //check if there is no card to display
            style = {display: 'none'};
        }
        else{
            style={display: ''};
        }
        if (typeof ( this.props.cardDescription)== 'undefined') {  //if no card
            cardToDisplay.hp="";

            cardToDisplay.energy= "";
            cardToDisplay.defence= "";
            cardToDisplay.attack= "";
            cardToDisplay.price= "";
            cardToDisplay.imgUrl= "";
            cardToDisplay.name= "";
            cardToDisplay.family= "";
            cardToDisplay.description= "";

        }
        else {
            cardToDisplay= this.props.cardDescription;
        }

        return (
            <div className="ui special cards" style={style}>
                <div className="card">

                    <div className="content">
                        <div className="ui grid">
                            <div className="three column row">
                                <div className="column">
                                    <i className="heart outline icon"/><span id="cardHPId">{Math.round(cardToDisplay.hp)}</span>
                                </div>
                                <div className="column">
                                    <h5>{cardToDisplay.family}</h5>
                                </div>
                                <div className="column">
                                    <span id="energyId">{Math.round(cardToDisplay.energy)}</span>
                                    <i className="lightning icon"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="image imageCard">
                        <div className="blurring dimmable image">
                            <div className="ui fluid image">
                                <a className="ui left corner label">
                                    {cardToDisplay.name}
                                </a>
                                <img id="cardImgId" className="ui centered image" src={cardToDisplay.imgUrl}/>
                            </div>
                        </div>
                    </div>

                    <div className="content">
                        <div className="ui form tiny">
                            <div className="field">
                                <label id="cardNameId"/>

                                <span id="cardDescriptionId"> {cardToDisplay.description}</span>


                            </div>
                        </div>
                    </div>

                    <div className="content">
                        <i className="heart outline icon"/><span id="cardHPId"> HP {Math.round(cardToDisplay.hp)}</span>
                        <div className="right floated ">
                            <span id="cardEnergyId">Energy {Math.round(cardToDisplay.energy)}</span>
                            <i className="lightning icon"/>

                        </div>
                    </div>

                    <div className="content">
                        <span className="right floated">
                                <span id="cardAttackId"> Attack {Math.round(cardToDisplay.attack)}</span>
                            <i className="wizard icon"/>
                        </span>
                        <i className="protect icon"/>
                        <span id="cardDefenceId">defence {Math.round(cardToDisplay.defence)}</span>
                    </div>

                    <div className="ui bottom attached button">
                        <i className="money icon"/>
                        Actual Value <span id="cardPriceId"> {cardToDisplay.price}$</span>
                    </div>

                </div>
            </div>


        );
    }
}
//function to call when a message is received
const mapStateToProps = (state) => {
    return {
        cardDescription : state.cardReducer
    }
};

export default  connect(mapStateToProps)(CardDisplay);