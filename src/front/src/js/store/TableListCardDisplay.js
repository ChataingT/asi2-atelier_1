import React from 'react';
import {CARD_DISPLAY_TYPES} from "../card/Card";
import Card from "../card/Card";


export class TableListCardDisplay extends React.Component
{
    constructor(props)
    {
        super(props);
        
        this.state = {
            cards_list: props.cards_list,
            cards_list_user_table:props.cards_list_user_table
        }
    }
    
    render()
    {
        let list_to_use;
        let list_to_display;
        let display;
        switch (this.props.storeChoice)
        {
            case "sell":
                list_to_use=this.props.cards_list_user_table;
            break;
            case "buy":
                list_to_use=this.props.cards_list; 
            break;
        }
        return ( <table class="ui selectable celled table" id="cardListId">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Family</th>
                        <th>HP</th>
                        <th>Energy</th>
                        <th>Defence</th>
                        <th>Attack</th>
                        <th>Price</th>
                        <th/>
                    </tr>
                </thead>
                <tbody>
    
         {list_to_use.map((card, key) =>
            (
                
                <Card
                    card={card}
                    display_type={CARD_DISPLAY_TYPES.ROW}
                    key={key}
                    storeChoice= {this.props.storeChoice}
                    money={this.props.money}
                />
            ))}
                </tbody>
                </table>
            
        )
    }
}

