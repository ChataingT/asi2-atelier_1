import React from 'react';
import {CARD_DISPLAY_TYPES} from "../card/Card";
import Card from "../card/Card";

export class DivListCardDisplay extends React.Component
{
    constructor(props)
    {
        super(props);
        
        this.state = {
            cards_list_user_div: this.props.cards_list_user_div
        }
    }
    
    render()
    {
        let card_list = this.props.cards_list_user_div.map(card =>
            (
                <Card
                    card={card}
                    display_type={CARD_DISPLAY_TYPES.SHORT}
                />
            )
        )

        return <div className="ui doubling four column grid">
            { card_list }
        </div>
    }
}

