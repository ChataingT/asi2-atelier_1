import axios from 'axios';

const storeReducer= (state={list_card:[], list_card_user:[],

    currentUser:{}},action) => {
    switch (action.type) {
        case 'SELECT_CURRENT_USER':
            return {
                currentUser:action.obj,
                list_card_user:action.list_card_user
            };

        case 'LOAD_CARD_LIST':
            return {
                list_card_user:action.obj.list_card_user,
                list_card:action.obj.list_card,
                currentUser:state.currentUser
            };

        case 'BUY_CARD':
            let tmp_cardList=[];

            state.currentUser.cardList.map((item) => tmp_cardList.push(item));

            state.currentUser.cardList.map((item,key) =>(tmp_cardList.push(item)));
            tmp_cardList.push(action.obj);

            let storeOrder= {
                user_id:state.currentUser.id,
                card_id:action.obj.id
            };

            axios
                .post("http://127.0.0.1:8082/buy", storeOrder)  //request to the back end
                .then(function (data) {
                    if (data.data === true){
                        let tmp_cardList=[];
                        state.currentUser.cardList.map((item,key) =>(tmp_cardList.push(item)));
                        tmp_cardList.push(action.obj);

                        state.currentUser.money = state.currentUser.money - action.obj.price; //update money
                        state.currentUser.cardList=tmp_cardList; //update ths list of cards of the current user
                        return {
                            currentUser:state.currentUser};
                    }
                })
                .catch(function (error) {});

            return state;

        case 'SELL_CARD':
            let tmp_cardList2=[];
            state.currentUser.cardList.map((item,key) =>(tmp_cardList2.push(item)));
            tmp_cardList2.splice(tmp_cardList2.indexOf(action.obj),1); //delete the card that we want to sell

            let storeOrderSell={
                user_id:state.currentUser.id,
                card_id:action.obj.id
            };

            axios
                .post("http://127.0.0.1:8082/sell", storeOrderSell)
                .then(function (data) {
                    if (data.data === true) {

                        let tmp_cardList2=[];

                        state.currentUser.cardList.map((item,key) =>(tmp_cardList2.push(item)));

                        tmp_cardList2.splice(tmp_cardList2.indexOf(action.obj),1);

                        state.currentUser.money = state.currentUser.money + action.obj.price; //update money
                        state.currentUser.cardList=tmp_cardList2; //update ths list of cards of the current user

                        return {
                           
                            currentUser:state.currentUser
                        };
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
            return state;


        default:
            return state;
    }
};

export default storeReducer;