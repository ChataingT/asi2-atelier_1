import React from 'react';

import {Card, CARD_DISPLAY_TYPES} from "../card/Card";
import {TableListCardDisplay} from "./TableListCardDisplay";
import {DivListCardDisplay} from "./DivListCardDisplay";
import { connect } from 'react-redux';

import * as data from '../../data/cards';

const CARD_LIST_DISPLAY_TYPE = {
    SHORT: 'short',
    ROW: 'row'
};

 class CardList extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            cards_list: this.props.cards_list,
            cards_list_user:this.props.cards_list_user,
            display_type: this.props.display_type,
            storeChoice : this.props.storeChoice
        }
    }
    
    render()
    {
        let display;
        switch (this.props.display_type)
        {
            case CARD_LIST_DISPLAY_TYPE.ROW:
                display = <TableListCardDisplay
                    cards_list={this.props.cards_list}
                    cards_list_user_table={this.props.cards_list_user}
                    storeChoice = {this.props.storeChoice}
                    money={this.props.money}
                />;
            break;
            case CARD_LIST_DISPLAY_TYPE.SHORT:
                display = <DivListCardDisplay
                    cards_list_user_div={this.props.cards_list_user}
                />;
            break;
        }
        
        return display;
    }
}

const mapStateToProps = (state) => {


    return {       
        cards_list_user:state.storeReducer.currentUser.cardList,
        cards_list:state.storeReducer.list_card}

    
};
//export default CardList;
export default  connect(mapStateToProps)(CardList);
