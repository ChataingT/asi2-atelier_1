export const sellOneCard=(card_obj)=>{
    return {
        type: 'SELL_CARD',
        obj:card_obj
        
    };
}

export const buyOneCard=(card_obj)=>{
    return {
        type: 'BUY_CARD',
        obj:card_obj
        
    };
}




export const loadCardList=(list_obj)=>{

    return {
        type: 'LOAD_CARD_LIST',
        obj:list_obj
        
    };
}