package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.Controller.CardModelService;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.user.model.UserModel;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CardModelService cardModelService;

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		return userRepository.findById(Integer.valueOf(id));
	}

	public Optional<UserModel> getUser(Integer id) {
		return userRepository.findById(id);
	}

	public void addUser(UserModel user) {
		// needed to avoid detached entity passed to persist error
		userRepository.save(user);
		List<CardModel> cardList=cardModelService.getRandCard(5);
		for(CardModel card: cardList) {
			user.addCard(card);
		}
		userRepository.save(user);
	}

	public void updateUser(UserModel user) {
		userRepository.save(user);

	}

	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		List<UserModel> ulist=null;
		ulist=userRepository.findByLoginAndPwd(login,pwd);
		return ulist;
	}
	
	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		UserModel user1 = new UserModel();
		user1.setLogin("John");
		user1.setPwd("John");
		user1.setAccount(1523);
		user1.setLastName("Doe");
		user1.setSurName("John");
		this.addUser(user1);
	
		UserModel user2 = new UserModel();
		user2.setLogin("test");
		user2.setPwd("test");
		user2.setAccount(6666);
		user2.setLastName("test");
		user2.setSurName("test");
		this.addUser(user2);
	

		UserModel user3 = new UserModel();
		user3.setLogin("B");
		user3.setPwd("B");
		user3.setAccount(100);
		user3.setLastName("B");
		user3.setSurName("B");
		this.addUser(user3);
		
		UserModel user4 = new UserModel();
		user4.setLogin("JF");
		user4.setPwd("JF");
		user4.setAccount(1000);
		user4.setLastName("JF");
		user4.setSurName("JF");
		this.addUser(user4);
	}

}
