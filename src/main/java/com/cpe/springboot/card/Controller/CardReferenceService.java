package com.cpe.springboot.card.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.nio.file.Files;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.cpe.springboot.card.model.CardReference;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class CardReferenceService {


	@Autowired
	private CardRefRepository cardRefRepository;

	public List<CardReference> getAllCardRef() {
		List<CardReference> cardRefList = new ArrayList<>();
		cardRefRepository.findAll().forEach(cardRefList::add);
		return cardRefList;
	}

	public void addCardRef(CardReference cardRef) {
		cardRefRepository.save(cardRef);
	}

	public void updateCardRef(CardReference cardRef) {
		cardRefRepository.save(cardRef);

	}

	public CardReference getRandCardRef() {
		List<CardReference> cardRefList=getAllCardRef();
		if( cardRefList.size()>0) {
			Random rand=new Random();
			int rindex=rand.nextInt(cardRefList.size()-1);
			return cardRefList.get(rindex);
		}
		return null;
	}

	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		String jsonFileListCards="/json/cards.json";
		File file = null;
	    try {
	    	file = new File(getClass().getResource("/json/cards.json").getFile());
	        //file = ResourceUtils.getFile(jsonFileListCards);
	        //Read File Content
	        String content = new String(Files.readAllBytes(file.toPath()));
			JsonParser springParser = JsonParserFactory.getJsonParser();
			Map<String, Object> map = springParser.parseMap(content);
	
			String mapArray[] = new String[map.size()];
			System.out.println(map.get("cards"));
			System.out.println("Items found: " + mapArray.length);
			Object list = map.get("cards");
			int size = ((List)list).size(); 
			for (int i = 0; i < size; i++) {
				Object cards = ((ArrayList)list).get(i);
				Object key = ((LinkedHashMap<String, Object>)cards).get("key");
				Object hp = ((LinkedHashMap<String, Object>)cards).get("hp");
				Object energy = ((LinkedHashMap<String, Object>)cards).get("energy");
				Object defense = ((LinkedHashMap<String, Object>)cards).get("defence");
				Object attack = ((LinkedHashMap<String, Object>)cards).get("attack");
				Object price = ((LinkedHashMap<String, Object>)cards).get("price");
				Object imgUrl = ((LinkedHashMap<String, Object>)cards).get("imgUrl");
				Object smallImgUrl = ((LinkedHashMap<String, Object>)cards).get("smallImgUrl");
				Object name = ((LinkedHashMap<String, Object>)cards).get("name");
				Object family = ((LinkedHashMap<String, Object>)cards).get("family");
				Object description = ((LinkedHashMap<String, Object>)cards).get("description");
				CardReference cardRef = new CardReference((String)name, (String)description, (String)family, "", (String)imgUrl, (String)smallImgUrl);
				//CardReference cardRef =new CardReference("name"+i,"description"+i,"family"+i,"affinity"+i,"http://medias.3dvf.com/news/sitegrab/gits2045.jpg","https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg");
				addCardRef(cardRef);
				
			}
			System.out.println(getAllCardRef());/*{
				
			}
			int i=0;
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				CardReference cardRef =new CardReference("name"+i,"description"+i,"family"+i,"affinity"+i,"http://medias.3dvf.com/news/sitegrab/gits2045.jpg","https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg");
				addCardRef(cardRef);
				System.out.println(entry.getKey() + " = " + entry.getValue());
				i++;
			}
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
		for(int i=0;i<10;i++){
			CardReference cardRef =new CardReference("name"+i,"description"+i,"family"+i,"affinity"+i,"http://medias.3dvf.com/news/sitegrab/gits2045.jpg","https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg");
			addCardRef(cardRef);
			i++;
		}*/
	}
	catch (FileNotFoundException e) {
	        e.printStackTrace();
	} catch (IOException e) {
	        e.printStackTrace();
	    }
	
	}
}
